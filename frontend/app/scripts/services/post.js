'use strict';

angular.module('projectApp')
    .factory('Post', function ($resource, API_URL) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var Post = $resource(API_URL + 'api/v1/post/', {}, {},
            {
                stripTrailingSlashes: false
            }
        );

        return Post;
    });