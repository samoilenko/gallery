'use strict';

/**
 * @ngdoc directive
 * @name projectApp.directive:directives/readImage
 * @description
 * # directives/readImage
 */
angular.module('projectApp')
  .directive('readImage', function ($http) {
    return {
//      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.bind('change', function(e){
            scope.$emit('file.ready', e.target.files[0]);
        });
      }
    };
  });
