'use strict';

/**
 * @ngdoc directive
 * @name projectApp.directive:directives/head
 * @description
 * # directives/head
 */
angular.module('projectApp')
  .directive('gHead', function (API_URL, $window, $interval, $http) {
    return {
      templateUrl: 'views/directives/head.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        scope.info = {
            postCount: 0,
            viewCount: 0
        };

        scope.$on('post.added', function(){
            scope.info.postCount++;
        });

        var getStatistic = function(){
          $http.get(API_URL + 'api/v1/statistic/')
              .then(function(res){
                  scope.info.postCount = res.data.postCount;
                  scope.info.viewCount = res.data.viewCount;
              });
        };
        getStatistic();
        var intervalId = $interval(getStatistic, 15000);

        element.on('$destroy', function() {
            $interval.cancel(intervalId);
        });

        scope.getCsv = function(){
            $window.open(API_URL + 'api/v1/post/csv');
        };
      }
    };
  });
