'use strict';

/**
 * @ngdoc directive
 * @name projectApp.directive:directives/postBox
 * @description
 * # directives/postBox
 */
angular.module('projectApp')
  .directive('gPostBox', function (Post) {
    return {
      templateUrl: 'views/directives/post.box.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
          scope.posts = Post.query();
          scope.$on('post.added', function(e, post){
              scope.posts.unshift(post);
          });
      }
    };
  });
