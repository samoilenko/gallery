'use strict';

/**
 * @ngdoc directive
 * @name projectApp.directive:directives/replyBox
 * @description
 * # directives/replyBox
 */
angular.module('projectApp')
  .directive('gReplyBox', function ($http, API_URL, Post) {
    return {
      templateUrl: 'views/directives/reply.box.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
          scope.post = new Post();

          scope.$on('file.ready', function(e, file){
              $http({
                    method: 'POST',
                    url: API_URL + 'api/v1/image/',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: file.slice(0, file.size)
                  })
                  .then(function(res){
                      scope.post.path = res.data;
                      return scope.post.$save();
                  })
                  .then(function(post){
                      scope.$emit('post.added', post);
                  })
                  .catch(function(e){
                      console.log(e);
                  })
              ;
          });
      }
    };
  });
