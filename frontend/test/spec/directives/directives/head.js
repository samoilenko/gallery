'use strict';

describe('Directive: directives/head', function () {

  // load the directive's module
  beforeEach(module('projectApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<directives/head></directives/head>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the directives/head directive');
  }));
});
