#!/bin/bash

if [ ! -f /project/package.json ];
then
    tail -f /dev/null && /bin/bash
else
    grunt serve
fi