<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/18/16
 * Time: 12:36 AM
 */

namespace Gallery\Controller;

use Gallery\Entity\PostEntity;
use Gallery\Entity\StatisticEntity;
use Gallery\Application;

class StatisticController {
    public function getAction(Application $app){
        $postCount = $app->getEntityManager()
                        ->getRepository(PostEntity::class)
                        ->createQueryBuilder('p')
                        ->select('count(p)')
                        ->getQuery()
                        ->getSingleScalarResult();

        /** @var StatisticEntity $statisticEntity */
        $statisticEntity = $app->getEntityManager()->getRepository(StatisticEntity::class)->findAll();

        return $app->json([
            'postCount' => $postCount,
            'viewCount' => $statisticEntity? $statisticEntity[0]->getViews(): 0,
        ]);
    }

    public function addAction(Application $app){
        $statisticEntity = $this->getStatisticEntity($app);
        $statisticEntity->setViews((int)$statisticEntity->getViews() + 1);

        $app->getEntityManager()->persist($statisticEntity);
        $app->getEntityManager()->flush();

        return $app->json($statisticEntity->toArray());
    }

    private function getStatisticEntity(Application $app){
        /** @var StatisticEntity $statisticEntity */
        $statisticEntity = $app->getEntityManager()->getRepository(StatisticEntity::class)->findAll();

        return !$statisticEntity? new StatisticEntity(): $statisticEntity[0];
    }
} 