<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/18/16
 * Time: 12:35 AM
 */

namespace Gallery\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class StatisticProvider implements ControllerProviderInterface{
    const CONTROLLER_NAME = 'api.statistic.controller';

    public function connect(Application $app) {
        $app[static::CONTROLLER_NAME] = $app->share(function() use ($app) {
            return new StatisticController();
        });

        /** @var ControllerCollection $controllers */
        $controllers = $app["controllers_factory"];

        $controllers
            ->get("/", static::CONTROLLER_NAME.":getAction");
        $controllers
            ->post("/", static::CONTROLLER_NAME.":addAction");

        return $controllers;
    }
}
