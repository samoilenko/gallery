<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 8:35 PM
 */

namespace Gallery\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class PostProvider implements ControllerProviderInterface{
    const CONTROLLER_NAME = 'api.post.controller';

    public function connect(Application $app) {
        $app[static::CONTROLLER_NAME] = $app->share(function() use ($app) {
            return new PostController();
        });

        /** @var ControllerCollection $controllers */
        $controllers = $app["controllers_factory"];

        $controllers
            ->post("/", static::CONTROLLER_NAME.":addAction");
        $controllers
            ->get("/", static::CONTROLLER_NAME.":getAction");
        $controllers
            ->get("/csv", static::CONTROLLER_NAME.":getCsvAction");

        return $controllers;
    }
}
