<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 8:35 PM
 */

namespace Gallery\Controller;


use Gallery\Application;
use Gallery\Component\HttpFoundation\CsvResponse;
use Gallery\Entity\PostEntity;
use Gallery\Form\PostForm;
use Gallery\Traits\Errors;
use Gallery\Traits\GetFormErrors;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PostController {
    use GetFormErrors, Errors;

    public function addAction(Application $app, Request $request){
        try {
            $post = new PostEntity();
            $form = $app->getFormFactory()
                ->create(
                    PostForm::class,
                    $post,
                    [
                        'method' => 'POST',
                        'excludePath' => $request->getHost() . '/'. $app->getConfigByName('imagePublicFolder'),
                        'imageFolder' => Application::ROOT_DIR . $app->getConfigByName('imageDestinationFolder'),
                    ]
                );

            $form->submit($request->request->all());

            if (!$form->isValid()) {
                $this->setErrors($this->getFormErrors($form));
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Data is not valid');
            }

            $app->getEntityManager()->persist($post);
            $app->getEntityManager()->flush();

            return $app->json($post->toArray());
        }catch(HttpException $e){
            $app->getMonolog()->addDebug($e);
            return $app->json($this->getErrors(), $e->getStatusCode());
        }
    }

    public function getAction(Application $app, Request $request){
        $res = [];
        /** @var PostEntity $post */
        foreach($app->getEntityManager()->getRepository(PostEntity::class)->findAll() as $post){
            $res[] = $post->toArray();
        }

        return $app->json($res);
    }

    public function getCsvAction(Application $app, Request $request){
        $data = [];
        /** @var PostEntity $post */
        foreach($app->getEntityManager()->getRepository(PostEntity::class)->findAll() as $post){
            $data[] = [$post->getTitle(), $post->getPath()];
        }

        $titles = ['title', 'path'];

        return (new CsvResponse(
            $data,
            $titles
        ));
    }
}