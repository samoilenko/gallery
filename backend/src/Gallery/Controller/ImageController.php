<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 4:21 PM
 */

namespace Gallery\Controller;

use Gallery\Application;
use Gallery\Common\Image;
use Gallery\Common\SaveImage;
use Symfony\Component\HttpFoundation\Request;

class ImageController {
    public function uploadAction(Application $app, Request $request){
        $folder = '/'.date('Y').'/'.date('n').'/';
        $filename = (new SaveImage(
                    Application::ROOT_DIR.$app->getConfigByName('imageDestinationFolder').$folder,
                    $app->getConfigByName('image.max.size'))
        )
            ->save(new Image($request->getContent()));

        return $request->getHost().'/'.$app->getConfigByName('imagePublicFolder').$folder.$filename;
    }
} 