<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 4:22 PM
 */

namespace Gallery\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class ImageProvider implements ControllerProviderInterface{
    const CONTROLLER_NAME = 'api.image.controller';

    public function connect(Application $app) {
        $app[static::CONTROLLER_NAME] = $app->share(function() use ($app) {
            return new ImageController();
        });

        /** @var ControllerCollection $controllers */
        $controllers = $app["controllers_factory"];

        $controllers
            ->post("/", static::CONTROLLER_NAME.":uploadAction");

        return $controllers;
    }
}