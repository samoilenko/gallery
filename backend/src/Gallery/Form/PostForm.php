<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 8:37 PM
 */

namespace Gallery\Form;

use Gallery\Entity\PostEntity;
use Gallery\Form\Constraints\PathFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PostForm extends AbstractType{
    const TITLE_MAX_LENGTH = 255;

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('title', TextType::class, array(
                'required'       => false,
                'invalid_message' => 'Invalid title name',
                'constraints'    => [
                    new Assert\Length([
                        'max' => self::TITLE_MAX_LENGTH,
                        'maxMessage' => sprintf('title max length is %d symbols', self::TITLE_MAX_LENGTH),
                        'groups'  => ['Default'],
                    ])
                ],
            ))
            ->add('path', TextType::class, [
                'required'    => true,
                'constraints' => [
                    new PathFile([
                        'excludePath' => $options['excludePath'],
                        'imageFolder' => $options['imageFolder'],
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class'            => PostEntity::class,
            'csrf_protection'       => false,
            'allow_extra_fields'    => true,
            'excludePath'           => '',
            'imageFolder'           => ''
        ));
    }

    public function getName(){
        return 'post';
    }
}
