<?php
namespace Gallery\Form\Constraints;

use Symfony\Component\Validator\Constraint;

class PathFile extends Constraint{
    public $message    = 'File doesn\'t exists';
    public $path;
    public $excludePath;
    public $imageFolder;

}