<?php
namespace Gallery\Form\Constraints;

use Silex\Application;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class PathFileValidator extends ConstraintValidator{
    public function validate($value, Constraint $constraint){
        $value = str_replace($constraint->excludePath, '', $value);
        if (!file_exists($constraint->imageFolder.'/'.$value)) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
//                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
//                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->addViolation();
            }
        }
    } 
}