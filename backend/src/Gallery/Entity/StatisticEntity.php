<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/18/16
 * Time: 12:59 AM
 */

namespace Gallery\Entity;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @Table(name="statistic")
 */
class StatisticEntity extends BaseEntity{
    /**
     * @Column(type="bigint")
     */
    protected $views;


    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

} 