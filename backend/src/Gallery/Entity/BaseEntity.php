<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 9:19 PM
 */

namespace Gallery\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;

abstract class BaseEntity{
    /**
     * @Id
     * @Column(type="bigint")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @Column(type="datetime")
     */
    protected $updated_at;

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;

        return $this;
    }

    public function setCreatedAt($date){
        $this->created_at = $date;

        return $this;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function setUpdatedAt($date){
        $this->updated_at = $date;

        return $this;
    }

    public function getUpdatedAt(){
        return $this->updated_at;
    }

    public function fill(array $param){
        foreach($this->toArray() as $k => $v){
            if(array_key_exists($k, $param)){
                $this->$k = is_string($param[$k])? trim($param[$k]): $param[$k];
            }
        }

        return $this;
    }

    public function toArray(){
        return get_object_vars($this);
    }

    /**
     *
     * @PrePersist
     * @PreUpdate
     */
    public function updatedTimestamps(){
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}
