<?php
namespace Gallery\Component\HttpFoundation;

use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvResponse extends StreamedResponse{
    protected $data;
    protected $filename;
    protected $delimiter;
    protected $titles;

    /**
     * @param null $data
     * @param array $titles
     * @param null $filename
     * @param string $delimiter
     * @param int $status
     * @param array $headers
     */
    public function __construct($data = null, array $titles = [], $filename = null, $delimiter = ';', $status = 200, $headers = array()){
        parent::__construct($this->getCallback(), $status, $headers);


        $this->filename  = $filename? : (new \DateTime())->format("d-m-Y");
        $this->delimiter = $delimiter;
        $this->data      = $data? (array)$data: new \ArrayObject();
        $this->titles    = $titles;

        $this->headers->set('Content-Type', 'text/csv');
        $this->headers->set('Content-Disposition', sprintf('attachment; filename=%s.csv', $this->filename));
        $this->headers->set('Pragma', 'no-cache');
        $this->headers->set('Expires', 0);
    }

    protected function getCallback(){
        return function(){
            $output = fopen("php://output", "w");

            $row = array_shift($this->data);
            fputcsv($output, $this->titles? :array_keys((array)$row), $this->delimiter);

            do{
                fputcsv($output, (array)$row, $this->delimiter);
            }while($row = array_shift($this->data));

            fclose($output);
        };
    }
}
