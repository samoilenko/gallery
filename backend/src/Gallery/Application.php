<?php
namespace Gallery;

use Gallery\Controller\ImageProvider;
use Gallery\Controller\PostProvider;
use Gallery\Controller\StatisticProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Silex\Provider;
use Silex\Application\SecurityTrait;
use Silex\Application\UrlGeneratorTrait;
use Doctrine\ORM\EntityManager;
use Saxulum\DoctrineOrmManagerRegistry\Silex\Provider\DoctrineOrmManagerRegistryProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class Application extends \Silex\Application{
    use UrlGeneratorTrait;

    const ROOT_DIR = __DIR__ . "/../../";

    private $_configValues = [];

    public function __construct($configFilename, $pathToConfig, array $values = array()){
        parent::__construct($values);

        $this->fillConfigFromFiles($configFilename, $pathToConfig);

        foreach($this->getConfigValues() as $key => $value) {
            $this[$key] = $value;
        }
    }

    public function setConfig($key, $value){
        $this->_configValues[$key] = $value;

        return $this;
    }

    /**
     * @param $configFilename
     * @param $pathToConfig
     * @return $this
     * @throws ParseException If the YAML is not valid
     */
    private function fillConfigFromFiles($configFilename, $pathToConfig){
        foreach ((array)$configFilename as $filename) {
            $this->_configValues = array_replace_recursive($this->_configValues, Yaml::parse($pathToConfig . $filename . '.yml'));
        }

        return $this;
    }


    public function getConfigValues(){
        return $this->_configValues;
    }

    /**
     * @param string  param1, param2, param3.....paramN
     *
     * @return mixed
     */
    public function getConfigByName(){
        if(func_num_args() == 0){
            $this->getMonolog()
                 ->addError('Empty parameters list');
            return false;
        }

        $config = $this->getConfigValues();
        foreach(func_get_args() as $v){
            if(!isset($config[$v])){
                $this->getMonolog()->addError(sprintf('Config value \'%s\' not found. Parameters list[%s]',
                                                      $v, implode(',', func_get_args())
                                           )
                );

                return false;
            }

            $config = $config[$v];
        }

        return $config;
    }

    public function getAvailableLanguages(){
        $languages = [];
        foreach(glob(__DIR__ . '/../../locale/*') as $locale) {
            $languages[] = basename($locale);
        }

        return $languages;
    }

    public function init(){
        $this->before(function (Request $request) {
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : array());
            }
        });

        $this->register(new Provider\ServiceControllerServiceProvider())
             ->register(new Provider\UrlGeneratorServiceProvider())
             ->register(new Provider\FormServiceProvider())
             ->register(new Provider\ValidatorServiceProvider())
             ->register(new DoctrineOrmManagerRegistryProvider())
             ->register(new Provider\DoctrineServiceProvider())
             ->register(new Provider\MonologServiceProvider(), $this->getConfigByName('monolog.config'))
            ->register(new DoctrineOrmServiceProvider(), [
                "orm.custom.functions.numeric" => [
                    "RAND" => "Broadway\\Bridge\\Doctrine2\\Functions\\Rand"
                ],
            ])
        ;


        $this->error(function (\Exception $e, $code){
            $this->getMonolog()->addError($e);

            $variables = [
                'message'          => $this->getErrorMessage($e, $code),
                'code'             => $code,
//                'validationErrors' => $this->getSession()->getFlashBag()->get('validationErrors'),
            ];

            return $this->json($variables);
        });

        return $this;
    }
    
    public function getErrorMessage(\Exception $e, $code){
        if(in_array($code, [Response::HTTP_METHOD_NOT_ALLOWED, Response::HTTP_NOT_FOUND])){
            return $e->getMessage();
        }

        if($e instanceof HttpExceptionInterface){
            return $e->getMessage();
        }

        return $e instanceof Base? $e->getMessage(): 'Sorry. We have an unexpected error. Please try later.';
    }

    public function initRoutes(){
        $this->mount('/api/v1/image',   new ImageProvider());
        $this->mount('/api/v1/post',   new PostProvider());
        $this->mount('/api/v1/statistic',   new StatisticProvider());

        return $this;
    }

    /**
     * @return \Monolog\Logger
     */
    public function getMonolog(){
        return $this['monolog'];
    }

    /**
    * @return \Symfony\Component\Form\FormFactoryInterface
    *
    */
    public function getFormFactory(){
        return $this['form.factory'];
    }

    /**
     * @return \Symfony\Component\Validator\Validator
     */
    public function getValidator(){
        return $this['validator'];
    }

    /**
     * @return EntityManager
     */
    public final function getEntityManager() {
        return $this['orm.em'];
    }

}