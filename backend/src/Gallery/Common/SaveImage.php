<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 7:39 PM
 */

namespace Gallery\Common;


use Gallery\Application;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SaveImage {
    private $directory;
    private $maxSize;

    public function __construct($directory, $maxSize){
        $this->directory = $directory;
        $this->maxSize   = $maxSize;
    }

    public function save(Image $image){
        if($image->getSize() > $this->maxSize){
            throw new BadRequestHttpException(sprintf("Image max size is %s", $this->maxSize));
        }

        if(!file_exists($this->directory)){
            if(!mkdir($this->directory, 0777, true)){
                throw new \Exception(sprintf('Can\'t create directory %s', $this->directory));
            }
        }

        $filename = time().'.'.strtolower($image->getExt());

        if(file_put_contents($this->directory.$filename, $image->getBlob()) === false){
            throw new \Exception(sprintf("Can\'t save file. Please try later. %s", $filename));
        }

        return $filename;
    }
} 