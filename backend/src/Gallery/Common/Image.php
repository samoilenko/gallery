<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/13/15
 * Time: 3:30 PM
 */

namespace Gallery\Common;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Image{
    private $img;
    private $ext;
    private $size;

    public function __construct($blobImage){
        if(empty($blobImage)){
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Image content is empty.');
        }

        $this->img = $this->createImage($blobImage);
    }

    public function getSize(){
        $this->size;
    }

    public function getBlob(){
        return $this->img;
    }

    private function createImage($image){
        try{
            $im = new \Imagick();

            $im->readimageblob($image);
            $this->ext = $im->getImageFormat();
            $this->size = $im->getimagesize();

            return $im->getimageblob();
        }
        catch (\Exception $e) {
            throw new Http('This is not supported a picture filetype.', Response::HTTP_BAD_REQUEST);

        }
        finally{
            $im->destroy();
        }
    }

    /**
     * @return string
     */
    protected function getFile(){
        return $this->img;
    }

    /**
     * @return String
     */
    public function getExt()
    {
        return $this->ext;
    }
}