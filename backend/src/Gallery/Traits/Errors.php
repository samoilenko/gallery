<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 4/17/16
 * Time: 10:49 PM
 */

namespace Gallery\Traits;

trait Errors {
    private $errors = [];

    public function addErrors($error){
        $this->errors[] = $error;

        return $this;
    }

    public function setErrors(array $data){
        $this->errors = $data;
    }

    public function getErrors($isClean = true){
        try{
            return $this->errors;
        }finally{
            if($isClean){
                $this->errors = [];
            }
        }
    }
}