<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = new  Gallery\Application(['prod', 'config'], __DIR__.'/../config/');

$app->init()->initRoutes();

$app->boot();

$app->run();